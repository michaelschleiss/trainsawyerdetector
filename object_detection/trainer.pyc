ó
r[c           @   sÛ   d  Z  d d l Z d d l Z d d l m Z d d l m Z d d l m Z d d l m	 Z	 d d l m
 Z d d l m Z d d	 l m Z d d
 l m Z e j j Z d   Z e e d  Z d   Z d d  Z d S(   ss   Detection model trainer.

This file provides a generic training method that can be used to train a
DetectionModel.
iÿÿÿÿN(   t   optimizer_builder(   t   preprocessor_builder(   t   batcher(   t   preprocessor(   t   standard_fields(   t   ops(   t   variables_helper(   t   model_deployc         C   sø   |   } t  j | t j j d  | t j j <| t j j } t  j |  } | | t j j <t j j | k }	 t j j | k }
 t j j | k } | rÍ t	 j
 | | d t	 j d | d |	 d |
  } n  t j | d |  d | d | d	 | } | S(
   sO  Sets up reader, prefetcher and returns input queue.

  Args:
    batch_size_per_clone: batch size to use per clone.
    create_tensor_dict_fn: function to create tensor dictionary.
    batch_queue_capacity: maximum number of elements to store within a queue.
    num_batch_queue_threads: number of threads to use for batching.
    prefetch_queue_capacity: maximum capacity of the queue used to prefetch
                             assembled batches.
    data_augmentation_options: a list of tuples, where each tuple contains a
      data augmentation function and a dictionary containing arguments and their
      values (see preprocessor.py).

  Returns:
    input queue: a batcher.BatchQueue object holding enqueued tensor_dicts
      (which hold images, boxes and targets).  To get a batch of tensor_dicts,
      call input_queue.Dequeue().
  i    t   func_arg_mapt   include_multiclass_scorest   include_instance_maskst   include_keypointst
   batch_sizet   batch_queue_capacityt   num_batch_queue_threadst   prefetch_queue_capacity(   t   tft   expand_dimst   fieldst   InputDataFieldst   imaget   to_floatt   groundtruth_instance_maskst   groundtruth_keypointst   multiclass_scoresR   t
   preprocesst   get_default_func_arg_mapR   t
   BatchQueue(   t   batch_size_per_clonet   create_tensor_dict_fnR   R   R   t   data_augmentation_optionst   tensor_dictt   imagest   float_imagesR
   R   R	   t   input_queue(    (    sF   /home/q435937/Projects/trainsawyerdetector/object_detection/trainer.pyt   create_input_queue&   s4    	 									c            s=   |  j    } d        f d   } t t | |    S(   s'  Dequeues batch and constructs inputs to object detection model.

  Args:
    input_queue: BatchQueue object holding enqueued tensor_dicts.
    num_classes: Number of classes.
    merge_multiple_label_boxes: Whether to merge boxes with multiple labels
      or not. Defaults to false. Merged boxes are represented with a single
      box and a k-hot encoding of the multiple labels associated with the
      boxes.
    use_multiclass_scores: Whether to use multiclass scores instead of
      groundtruth_classes.

  Returns:
    images: a list of 3-D float tensor of images.
    image_keys: a list of string keys for the images.
    locations_list: a list of tensors of shape [num_boxes, 4]
      containing the corners of the groundtruth boxes.
    classes_list: a list of padded one-hot tensors containing target classes.
    masks_list: a list of 3-D float tensors of shape [num_boxes, image_height,
      image_width] containing instance masks for objects if present in the
      input_queue. Else returns None.
    keypoints_list: a list of 3-D float tensors of shape [num_boxes,
      num_keypoints, 2] containing keypoints for objects if present in the
      input queue. Else returns None.
    weights_lists: a list of 1-D float32 tensors of shape [num_boxes]
      containing groundtruth weight for each box.
  i   c   	         s  |  t  j j } d } t  j j |  k r; |  t  j j } n  |  t  j j } t j |  t  j j t j  } |   8}  r  r t	 d   n   r¶ t
 j | |   \ } } } nF  rÞ t j |  t  j j t j  } n t
 j d | d  d d  } |  j t  j j  } |  j t  j j  }  rS| d k	 sD| d k	 rSt d   n  |  j t  j j  } | | | | | | | f S(	   s/   Extract images and targets from the input dict.t    sO   Using both merge_multiple_label_boxes and use_multiclass_scores isnot supportedt   indicest   deptht   left_padi    s&   Multi-label support is only for boxes.N(   R   R   R   t	   source_idt   groundtruth_boxesR   t   castt   groundtruth_classest   int32t
   ValueErrort   util_opst    merge_boxes_with_multiple_labelsR   t   float32t   padded_one_hot_encodingt   getR   R   t   Nonet   NotImplementedErrort   groundtruth_weights(	   t	   read_dataR   t   keyt   location_gtt
   classes_gtt   _t   masks_gtt   keypoints_gtt
   weights_gt(   t   label_id_offsett   merge_multiple_label_boxest   num_classest   use_multiclass_scores(    sF   /home/q435937/Projects/trainsawyerdetector/object_detection/trainer.pyt   extract_images_and_targets|   s8    
	(   t   dequeuet   zipt   map(   R"   R@   R?   RA   t   read_data_listRB   (    (   R>   R?   R@   RA   sF   /home/q435937/Projects/trainsawyerdetector/object_detection/trainer.pyt
   get_inputs[   s    $c         C   sO  |   } t  |  | j | j | j  \ } } } } } }	 } g  }
 g  } x= | D]5 } | j |  \ } } |
 j |  | j |  qO Wt j |
 d  } t j | d  } t d   | D  rË d } n  t d   |	 D  rê d }	 n  | j
 | | | |	  | j | |  } | j | |  } x$ | j   D] } t j j |  q1Wd S(   së   Creates loss function for a DetectionModel.

  Args:
    input_queue: BatchQueue object holding enqueued tensor_dicts.
    create_model_fn: A function to create the DetectionModel.
    train_config: a train_pb2.TrainConfig protobuf.
  i    c         s   s   |  ] } | d  k Vq d  S(   N(   R3   (   t   .0t   mask(    (    sF   /home/q435937/Projects/trainsawyerdetector/object_detection/trainer.pys	   <genexpr>½   s    c         s   s   |  ] } | d  k Vq d  S(   N(   R3   (   RH   t	   keypoints(    (    sF   /home/q435937/Projects/trainsawyerdetector/object_detection/trainer.pys	   <genexpr>¿   s    N(   RG   R@   R?   RA   R   t   appendR   t   concatt   anyR3   t   provide_groundtrutht   predictt   losst   valuest   lossest   add_loss(   R"   t   create_model_fnt   train_configt   detection_modelR    R:   t   groundtruth_boxes_listt   groundtruth_classes_listt   groundtruth_masks_listt   groundtruth_keypoints_listt   preprocessed_imagest   true_image_shapesR   t   resized_imaget   true_image_shapet   prediction_dictt   losses_dictt   loss_tensor(    (    sF   /home/q435937/Projects/trainsawyerdetector/object_detection/trainer.pyt   _create_losses£   s4    	!			c   .   #      s«  |   } g   j  D] } t j |  ^ q } t j   j   et j d | d | d | d | d | d |	  } t j | j	     t
 j   } Wd QXt j | j    0 t  j | |   j  j  j |  } Wd QXt t j t j j   } t g   } t j t d | d	  } t j | | | g  } | d
 j } | rst j | j	     |   Wd QXn  t j t j j |  } t j | j    M t j  j  \ } } x- | D]% } t j j  | j! j" | d d qÀWWd QXd }  j$ r(t j% j& | d  j' d | } | } n  t j | j    @ j( rMd n g  } t j) | | d | \ } } t j* | d  }  j+ r³d g }  t, j- | |  d  j+ } n   j. rÔt, j/ |  j.  } n   j0 d
 k rt j1 d   t
 j2 j3 |  j0  } Wd QXn  | j4 | d | }! | j5 |!  t j6 d d |  }" t j7 |" g   t j8 | d d }# Wd QXWd QXx: t
 j9   D], }$ | j: t j j; d |$ j! j" |$   qWx= t j< j=   D], }% | j: t j j  d |% j! j" |%   qÌW| j: t j j  d t j< j>     | t t j t j j |   O} | | O} t j j? t@ |  d d }& t jA d tB d tC  }'  jD }( t j% jE d |(  }) d }*  jF r: jG sÚ jH rÎd  _G qÚd   _G n  | jI d!  jG d"  jJ  }+ t, jK |+  jF  }, t j% jE |,       f d#   }- |- }* n  t
 j2 j% |# d$ | d% | d& |
 d' |' d(  jL d) |* d |& d*  jM r jM n d d+ d, d- | d. |) Wd QXd S(/   s+  Training function for detection models.

  Args:
    create_tensor_dict_fn: a function to create a tensor input dictionary.
    create_model_fn: a function that creates a DetectionModel and generates
                     losses.
    train_config: a train_pb2.TrainConfig protobuf.
    master: BNS name of the TensorFlow master to use.
    task: The task id of this training instance.
    num_clones: The number of clones to run per machine.
    worker_replicas: The number of work replicas to train with.
    clone_on_cpu: True if clones should be forced to run on CPU.
    ps_tasks: Number of parameter server tasks.
    worker_job_name: Name of the worker job.
    is_chief: Whether this replica is the chief replica.
    train_dir: Directory to write checkpoints and training summaries to.
    graph_hook_fn: Optional function that is called after the inference graph is
      built (before optimization). This is helpful to perform additional changes
      to the training graph such as adding FakeQuant ops. The function should
      modify the default graph.
  t
   num_clonest   clone_on_cput
   replica_idt   num_replicast   num_ps_taskst   worker_job_nameNRT   RU   i    t   familyt   LearningRatet   replicas_to_aggregatet   total_num_replicast   regularization_lossess   LossTensor is inf or nan.s	   .*/biasest
   multipliert
   clip_gradst   global_stept   namet   update_barriert   train_ops
   ModelVars/s   Losses/s   Losses/TotalLosst
   summary_opt   allow_soft_placementt   log_device_placementt   keep_checkpoint_every_n_hourst	   detectiont   classificationt   fine_tune_checkpoint_typet"   load_all_detection_checkpoint_varsc            s     j  |   j  d  S(   N(   t   restoret   fine_tune_checkpoint(   t   sess(   t
   init_saverRU   (    sF   /home/q435937/Projects/trainsawyerdetector/object_detection/trainer.pyt   initializer_fn~  s    t   logdirt   mastert   is_chieft   session_configt   startup_delay_stepst   init_fnt   number_of_stepst   save_summaries_secsix   t   sync_optimizert   saver(N   R   R   t   buildR   t   Grapht
   as_defaultR   t   DeploymentConfigt   devicet   variables_devicet   slimt   create_global_stept   inputs_deviceR#   R   R   R   R   t   sett   get_collectiont	   GraphKeyst	   SUMMARIESt	   functoolst   partialRb   t   create_clonest   scopet
   UPDATE_OPSt   optimizer_deviceR    t	   optimizert   summaryt   scalart   opRq   R3   t   sync_replicast   traint   SyncReplicasOptimizerRk   t   add_regularization_losst   optimize_clonest   check_numericst   bias_grad_multiplierR   t!   multiply_gradients_matching_regext   freeze_variablest   freeze_gradients_matching_regext   gradient_clipping_by_normt
   name_scopet   learningt   clip_gradient_normst   apply_gradientsRK   t   groupt   control_dependenciest   identityt   get_model_variablest   addt	   histogramRR   t
   get_lossest   get_total_losst   merget   listt   ConfigProtot   Truet   FalseRw   t   SaverR}   Rz   t   from_detection_checkpointt   restore_mapR{   t%   get_variables_available_in_checkpointR   t	   num_steps(.   R   RT   RU   R   t   taskRc   t   worker_replicasRd   t   ps_tasksRh   R   t	   train_dirt   graph_hook_fnRV   t   stepR   t   deploy_configRp   R"   t	   summariest   global_summariest   model_fnt   clonest   first_clone_scopet
   update_opst   training_optimizert   optimizer_summary_varst   varR   Rm   t
   total_losst   grads_and_varst   biases_regex_listt   grad_updatest	   update_opt   train_tensort	   model_varRa   Rt   R   Rw   R   R   t   var_mapt   available_var_mapR   (    (   R   RU   sF   /home/q435937/Projects/trainsawyerdetector/object_detection/trainer.pyR£   Í   sà    #	"			)											!
											(   t   __doc__R   t
   tensorflowR   t   object_detection.buildersR    R   t   object_detection.coreR   R   R   R   t   object_detection.utilsR   R.   R   t
   deploymentR   t   contribR   R#   R½   RG   Rb   R3   R£   (    (    (    sF   /home/q435937/Projects/trainsawyerdetector/object_detection/trainer.pyt   <module>   s    	7E	6